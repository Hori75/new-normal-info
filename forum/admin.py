from django.contrib import admin
from .models import PostNode, Topic

admin.site.register(Topic)
admin.site.register(PostNode)