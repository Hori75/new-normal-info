from django.contrib.auth.models import User
from django.test import LiveServerTestCase, TestCase, tag
from django.test.client import RequestFactory
from django.urls import reverse
from selenium import webdriver

from .models import Topic, PostNode
from .views import post, reply


@tag('functional')
class FunctionalTestCase(LiveServerTestCase):
    """Base class for functional test cases with selenium."""

    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        # Change to another webdriver if desired (and update CI accordingly).
        options = webdriver.chrome.options.Options()
        # These options are needed for CI with Chromium.
        options.headless = True  # Disable GUI.
        options.add_argument('--no-sandbox')
        options.add_argument('--disable-dev-shm-usage')
        cls.selenium = webdriver.Chrome(options=options)

    @classmethod
    def tearDownClass(cls):
        cls.selenium.quit()
        super().tearDownClass()


class ForumTestCase(TestCase):
    def setUp(self):
        self.factory = RequestFactory()
        self.user = User.objects.create_user(username='Dummy', email="dummy@works.com", password='Blablabla123')

    def test_root_url_status_200(self):
        content = PostNode(content="This is the message", post=None)
        content.save()
        topic = Topic(title="This is title", head=content)
        topic.save()
        response = self.client.get(reverse('forum:index'))
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, "forum/index.html")
        self.assertContains(response, "This is title")
    
    def test_posts_sort_by_popular(self):
        content1 = PostNode(content="This is the message", post=None)
        content1.save()
        topic1 = Topic(title="This is title", head=content1)
        topic1.save()
        content2 = PostNode(content="This is the message2", post=None)
        content2.save()
        topic2 = Topic(title="This is title2", head=content2)
        topic2.save()
        content3 = PostNode(content="This is reply of the message2", post=content2)
        content3.save()
        content3.update_topic()
        top_topic = Topic.objects.all().order_by("-reply_count").first()
        self.assertEqual(top_topic, topic2)
        response = self.client.get(reverse('forum:sortPost', args=["popular"]))
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, "forum/index.html")

    def test_posts_sort_by_recent(self):
        content1 = PostNode(content="This is the message", post=None)
        content1.save()
        topic1 = Topic(title="This is title", head=content1)
        topic1.save()
        content2 = PostNode(content="This is the message2", post=None)
        content2.save()
        topic2 = Topic(title="This is title2", head=content2)
        topic2.save()
        content3 = PostNode(content="This is reply of the message2", post=content2)
        content3.save()
        content3.update_topic()
        content4 = PostNode(content="This is reply of the message2", post=content1)
        content4.save()
        content4.update_topic()
        recent_topic = Topic.objects.all().order_by("-recent_update").first()
        self.assertEqual(recent_topic, topic1)
        response = self.client.get(reverse('forum:sortPost', args=["recent"]))
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, "forum/index.html")

    def test_view_topic_status_200(self):
        content = PostNode(content="This is the message", post=None)
        content.save()
        topic = Topic(title="This is title", head=content)
        topic.save()
        response = self.client.get(reverse('forum:view', args=[1]))
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, "forum/view.html")
        self.assertContains(response, "This is title")
        self.assertContains(response, "This is the message")

    def test_post_topic_get_login_required(self):
        response = self.client.get(reverse('forum:post'))
        self.assertRedirects(response, reverse('account:login')+"?next="+reverse('forum:post'))

    def test_post_topic_get_status_200(self):
        request = self.factory.get(reverse('forum:post'))
        request.user = self.user
        response = post(request)
        self.assertEqual(response.status_code, 200)
        self.assertContains(response, "Title")
        self.assertContains(response, "Content")
        self.assertContains(response, "Post")

    def test_post_topic_get_login_required(self):
        response = self.client.post(reverse('forum:post'))
        self.assertRedirects(response, reverse('account:login')+"?next="+reverse('forum:post'))

    def test_post_topic_post_no_data(self):
        request = self.factory.post(reverse('forum:post'))
        request.user = self.user
        response = post(request)
        self.assertEqual(response.status_code, 200)

    def test_post_topic_post_success(self):
        request = self.factory.post(reverse('forum:post'), data={
            "title" : "This is title",
            "content" : "This is the message"
        })
        request.user = self.user
        response = post(request)
        self.assertEqual(response.status_code, 302)

    def test_reply_get_success(self):
        content = PostNode(content="This is the message", post=None)
        content.save()
        topic = Topic(title="This is title", head=content)
        topic.save()
        request = self.factory.get(reverse('forum:reply', args=[1]))
        request.user = self.user
        response = reply(request, 1)
        self.assertEqual(response.status_code, 200)
        self.assertContains(response, "This is the message")
        self.assertContains(response, "Content")
        self.assertContains(response, "Balas")

    def test_reply_get_login_required(self):
        content = PostNode(content="This is the message", post=None)
        content.save()
        topic = Topic(title="This is title", head=content)
        topic.save()
        response = self.client.get(reverse('forum:reply', args=[1]))
        self.assertRedirects(response, reverse('account:login')+"?next="+reverse('forum:reply', args=[1]))

    def test_reply_post_login_required(self):
        content = PostNode(content="This is the message", post=None)
        content.save()
        topic = Topic(title="This is title", head=content)
        topic.save()
        response = self.client.post(reverse('forum:reply', args=[1]))
        self.assertRedirects(response, reverse('account:login')+"?next="+reverse('forum:reply', args=[1]))

    def test_reply_post_no_data(self):
        content = PostNode(content="This is the message", post=None)
        content.save()
        topic = Topic(title="This is title", head=content)
        topic.save()
        request = self.factory.post(reverse('forum:reply', args=[1]))
        request.user = self.user
        response = reply(request, 1)
        self.assertEqual(response.status_code, 200)

    def test_reply_post_success(self):
        content = PostNode(content="This is the message", post=None)
        content.save()
        topic = Topic(title="This is title", head=content)
        topic.save()
        request = self.factory.post(reverse('forum:reply', args=[1]), data={
            'content' : 'This is the reply of the post'
        })
        request.user = self.user
        response = reply(request, 1)
        self.assertEqual(response.status_code, 302)



class ForumFunctionalTestCase(FunctionalTestCase):
    def test_root_url_exists(self):
        self.selenium.get(f'{self.live_server_url}/forum/')
        html = self.selenium.find_element_by_tag_name('html')
        self.assertNotIn('not found', html.text.lower())
        self.assertNotIn('error', html.text.lower())