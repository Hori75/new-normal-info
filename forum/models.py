from ckeditor.fields import RichTextField
from django import forms
from django.contrib.auth.models import User
from django.db import models
from django.utils import timezone

class PostNode(models.Model):
    content = RichTextField(null=True)
    author = models.ForeignKey(User, on_delete=models.SET_NULL, null=True)
    post = models.ForeignKey('self', null=True, on_delete=models.CASCADE)
    time_creation = models.DateTimeField(auto_now_add=True)
    hidden = models.BooleanField(default=False)

    def get_topic(self):
        postnode = self
        while (postnode.post != None):
            postnode = postnode.post
        return postnode.topic

    def update_topic(self):
        topic = self.get_topic()
        topic.replyCountUp()
        return topic

    def has_replies(self):
        return self.postnode_set.first() is not None

    def get_creation_datetime(self):
        time = timezone.now()

        if (self.time_creation.year == time.year):
            if (self.time_creation.month == time.month):
                if (self.time_creation.day == time.day):
                    if (self.time_creation.hour == time.hour):
                        if (self.time_creation.minute == time.minute):
                            return str(time.second - self.time_creation.second) + " detik lalu"
                        else:
                            return str(time.minute - self.time_creation.minute) + " menit lalu"
                    else:
                        return str(time.hour - self.time_creation.hour) + " jam lalu"
                else:
                    return str(time.day - self.time_creation.day) + " hari lalu"
            else:
                return str(time.month - self.time_creation.month) + " bulan lalu"
        else:
            return self.time_creation

class Topic(models.Model):
    title = models.CharField(max_length=50)
    author = models.ForeignKey(User, on_delete=models.SET_NULL, null=True)
    head = models.OneToOneField(PostNode, on_delete=models.CASCADE)
    recent_update = models.DateTimeField(auto_now=True)
    time_creation = models.DateTimeField(auto_now_add=True)
    reply_count = models.BigIntegerField(default=0)

    def replyCountUp(self):
        self.reply_count += 1
        self.save()

    def get_recent_datetime(self):
        time = timezone.now()

        if (self.recent_update.year == time.year):
            if (self.recent_update.month == time.month):
                if (self.recent_update.day == time.day):
                    if (self.recent_update.hour == time.hour):
                        if (self.recent_update.minute == time.minute):
                            return str(time.second - self.recent_update.second) + " detik lalu"
                        else:
                            return str(time.minute - self.recent_update.minute) + " menit lalu"
                    else:
                        return str(time.hour - self.recent_update.hour) + " jam lalu"
                else:
                    return str(time.day - self.recent_update.day) + " hari lalu"
            else:
                return str(time.month - self.recent_update.month) + " bulan lalu"
        else:
            return self.recent_update

class TopicForm(forms.ModelForm):
    class Meta:
        model = Topic
        fields = ['title']

class PostNodeForm(forms.ModelForm):
    class Meta:
        model = PostNode
        fields = ['content']
