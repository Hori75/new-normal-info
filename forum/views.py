from django.contrib.auth.decorators import login_required
from django.http import HttpResponseNotFound
from django.shortcuts import redirect, render, reverse

from .models import PostNode, PostNodeForm, Topic, TopicForm

def index(request, ordering="recent"):
    if (ordering == "popular"):
        topics = Topic.objects.all().order_by("-reply_count")
    elif (ordering == "recent"):
        topics = Topic.objects.all().order_by("-recent_update")
    else:
        return HttpResponseNotFound()
    return render(request, "forum/index.html", {'topics' : topics})

def view(request, id):
    topic = Topic.objects.filter(pk=id).first()
    if (topic == None):
        # error(request, "Matkul tidak ditemukan!")
        return redirect(reverse("forum:index"))
    else:
        return render(request, "forum/view.html", {'topic' : topic})

@login_required
def post(request):
    if (request.method == "POST"):
        topicform = TopicForm(request.POST or None)
        postnodeform = PostNodeForm(request.POST or None)
        if (topicform.is_valid() and postnodeform.is_valid()):
            topic = topicform.save(commit=False)
            postnode = postnodeform.save(commit=False)
            postnode.hidden = False
            postnode.user = request.user
            postnode.save()
            topic.head = postnode
            topic.author = request.user
            topic.save()
            # success(request, "Matkul telah ditambahkan!")
            return redirect(reverse('forum:index'))
        else:
            return render(request, 'forum/post.html', {'topicform' : topicform, 'postnodeform' : postnodeform})
    else:
        topicform = TopicForm()
        postnodeform = PostNodeForm()
    return render(request, 'forum/post.html', {'topicform' : topicform, 'postnodeform' : postnodeform})

@login_required
def reply(request, id):
    postnodedest = PostNode.objects.filter(pk=id).first()
    if (postnodedest == None):
        # error(request, "Matkul tidak ditemukan!")
        return redirect(reverse("forum:index"))
    temp = postnodedest
    if (request.method == "POST"):
        postnodeform = PostNodeForm(request.POST or None)
        if (postnodeform.is_valid()):
            postnode = postnodeform.save(commit=False)
            postnode.post = postnodedest
            postnode.author = request.user
            postnode.hidden = False
            postnode.save()
            # success(bla blabal)
            return redirect(reverse("forum:view", args=[postnode.update_topic().pk]))
        else:
            return render(request, "forum/reply.html", {"postnodeform" : postnodeform, "postnodedest" : postnodedest})
    else:
        postnodeform = PostNodeForm()
        return render(request, "forum/reply.html", {"postnodeform" : postnodeform, "postnodedest" : postnodedest})

