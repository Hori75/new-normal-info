from django.contrib.auth.models import User
from django.test import LiveServerTestCase, TestCase, tag
from django.test.client import RequestFactory
from django.urls import reverse
from selenium import webdriver

from .views import login_view, signUp_view

@tag('functional')
class FunctionalTestCase(LiveServerTestCase):
    """Base class for functional test cases with selenium."""

    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        # Change to another webdriver if desired (and update CI accordingly).
        options = webdriver.chrome.options.Options()
        # These options are needed for CI with Chromium.
        options.headless = True  # Disable GUI.
        options.add_argument('--no-sandbox')
        options.add_argument('--disable-dev-shm-usage')
        cls.selenium = webdriver.Chrome(options=options)

    @classmethod
    def tearDownClass(cls):
        cls.selenium.quit()
        super().tearDownClass()


class AccountTestCase(TestCase):
    def setUp(self):
        self.factory = RequestFactory()
        self.user = User.objects.create_user(username='Dummy', email="dummy@works.com", password='Blablabla123')

    def test_login_url_status_200(self):
        response = self.client.get(reverse('account:login'))
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, "account/login.html")
        self.assertContains(response, "Username")
        self.assertContains(response, "Password")
        self.assertContains(response, "Login")

    def test_login_post_no_data(self):
        response = self.client.post(reverse('account:login'))
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, "account/login.html")

    def test_login_post_incorrect(self):
        response = self.client.post(reverse('account:login'), data={
            "username" : "Dummy",
            "password" : "Thisiswrong"
        })
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, "account/login.html")
        self.assertContains(response, "Your username or password is incorrect.")

    def test_login_post_success(self):
        response = self.client.post(reverse('account:login'), data={
            "username" : "Dummy",
            "password" : "Blablabla123"
        })
        self.assertRedirects(response, reverse("forum:index"))

    def test_login_url_already_authenticated(self):
        request = self.client.get(reverse('account:login'))
        request.user = self.user
        response = login_view(request)
        self.assertEqual(response.status_code, 302)

    def test_signup_url_status_200(self):
        response = self.client.get(reverse('account:signup'))
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, "account/signup.html")
        self.assertContains(response, "Username")
        self.assertContains(response, "Email")
        self.assertContains(response, "Password")
        self.assertContains(response, "Password confirmation")
        self.assertContains(response, "Daftar")

    def test_signup_post_no_data(self):
        response = self.client.post(reverse('account:signup'))
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, "account/signup.html")

    def test_signup_post_same_email(self):
        response = self.client.post(reverse('account:signup'), data={
            "username" : "Dummy2",
            "email" : "dummy@works.com",
            "password1" : "ThisShouldbeGood1245",
            "password2" : "ThisShouldbeGood1245"
        })
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, "account/signup.html")
        self.assertContains(response, "There is an existing account with this email")

    def test_signup_post_success(self):
        response = self.client.post(reverse('account:signup'), data={
            "username" : "Dummy2",
            "email" : "dummy2@works.com",
            "password1" : "ThisShouldbeGood1245",
            "password2" : "ThisShouldbeGood1245"
        })
        self.assertRedirects(response, reverse('forum:index'))

    def test_signup_url_already_authenticated(self):
        request = self.client.get(reverse('account:signup'))
        request.user = self.user
        response = signUp_view(request)
        self.assertEqual(response.status_code, 302)


class AccountFunctionalTestCase(FunctionalTestCase):
    def test_login_url_exists(self):
        self.selenium.get(f'{self.live_server_url}/account/login/')
        html = self.selenium.find_element_by_tag_name('html')
        self.assertNotIn('not found', html.text.lower())
        self.assertNotIn('error', html.text.lower())

    def test_signup_url_exists(self):
        self.selenium.get(f'{self.live_server_url}/account/signup/')
        html = self.selenium.find_element_by_tag_name('html')
        self.assertNotIn('not found', html.text.lower())
        self.assertNotIn('error', html.text.lower())