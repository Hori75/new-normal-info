from django.urls import path

from . import views

app_name = "account"

urlpatterns = [
    path('login/', views.login_view, name="login"),
    path('signup/', views.signUp_view, name="signup"),
    path('logout/', views.logout_view, name="logout")
]