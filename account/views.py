from django.contrib.auth import login, authenticate, logout
from django.shortcuts import redirect, render, reverse

from .models import UserCreationForm

def signUp_view(request):
    if (request.user.is_authenticated):
        return redirect(reverse('forum:index'))
    if request.method == 'POST':
        form = UserCreationForm(request.POST)
        if form.is_valid():
            form.save()
            username = form.cleaned_data.get('username')
            raw_password = form.cleaned_data.get('password1')
            user = authenticate(username=username, password=raw_password)
            login(request, user)
            return redirect(reverse('forum:index'))
    else:
        form = UserCreationForm()
    return render(request, 'account/signup.html', {'form': form})

def login_view(request):
    if (request.user.is_authenticated):
        return redirect(reverse('forum:index'))
    elif (request.method == "POST"):
        username = request.POST.get('username')
        password = request.POST.get('password')
        user = authenticate(request, username=username, password=password)
        if (user is not None):
            login(request, user)
            return redirect(reverse('forum:index'))
        else:
            return render(request, 'account/login.html', { "error" : "Your username or password is incorrect."})
    else: 
        return render(request, 'account/login.html')

def logout_view(request):
    logout(request)
    return redirect(reverse('forum:index'))