from django.contrib import admin
from .models import RS, City

admin.site.register(RS)
admin.site.register(City)
