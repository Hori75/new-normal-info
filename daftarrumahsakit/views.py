from django.http.response import HttpResponseRedirect, HttpResponse
from django.shortcuts import render
from .models import RS, City
from django.contrib import messages
from django.db.models import Q

def home(request):
    return render(request, 'daftarrumahsakit/home.html', {"city": City.objects.all()})
    
def single_slug(request, single_slug):
    allrs = RS.objects.filter(city__city_slug=single_slug)
    return render(request, 'daftarrumahsakit/detail.html',{"rs": allrs})

def search(request):
    if request.method == "POST":
        srch = request.POST['srh']
        if srch:
            match = RS.objects.filter(Q(name__icontains=srch)|Q(loc__icontains=srch))
            if match:
                return render(request, 'daftarrumahsakit/search.html', {'sr':match})
            else:
                messages.error(request, 'no result found')
        else:
            return HttpResponseRedirect('../')
    return render(request, 'daftarrumahsakit/search.html')

