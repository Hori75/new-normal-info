from django.db import models
# Create your models here.

class City(models.Model):
    name = models.CharField(max_length=150)
    city_slug = models.CharField(max_length=150, default="Jakarta")
    img = models.CharField(max_length=150, default="gambar")
    def __str__(self):
        return self.name

class RS(models.Model):
    name = models.CharField(max_length=150)
    loc = models.CharField(max_length=250)
    contact = models.CharField(max_length=12)
    city = models.ForeignKey(City, related_name='rs', on_delete=models.CASCADE, default=1)

    def __str___(self):
        return self.name