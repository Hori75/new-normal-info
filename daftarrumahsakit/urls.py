from django.urls import path
from . import views

app_name = 'daftarrumahsakit'

urlpatterns = [
    path('', views.home, name='home'),
    path('<single_slug>', views.single_slug, name='single_slug'),
    path('search/', views.search, name='search'),
]