from django.apps import AppConfig


class LearningplaceappConfig(AppConfig):
    name = 'learningplaceApp'
