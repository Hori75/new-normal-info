from django import forms

from .models import contactUs



class contactUsForm(forms.ModelForm):
    class Meta:
        model = contactUs
        fields = ('name', 'email', 'subject', 'message')
        labels  = {
        'name':'Your Name', 
        'email':'Your Email', 
        'subject':'Subject', 
        'message':'Message'
        }