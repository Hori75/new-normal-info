from django.db import models

# Create your models here.
class contactUs(models.Model):
    name = models.CharField(max_length=120)
    email = models.CharField(max_length=120, null=True)
    subject = models.CharField(max_length=120, blank=False, null=False)
    message = models.TextField(max_length=120, blank=False, null=False)