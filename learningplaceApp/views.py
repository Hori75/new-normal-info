from django.http import HttpResponseRedirect
from django.shortcuts import render


from .forms import contactUsForm
from .models import contactUs
# Create your views here.

def index(request):
    form = contactUsForm(request.POST or None)
 
    if form.is_valid():
        form.save()
        form = contactUsForm()

    context = {
        'form': form
    }
  
    return render(request, "learningplaceApp/index.html", context)
