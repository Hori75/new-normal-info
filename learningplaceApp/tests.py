from django.test import TestCase
from django.test import Client
from django.urls import resolve, reverse

from .models import contactUs
from .forms import contactUsForm
from . import views
from .views import index

# Create your tests here.

class UnitTestForLearningPlaceApp(TestCase):
    def test_response_page(self):
        response = Client().get(reverse("learningplace:index"))
        self.assertEqual(response.status_code, 200)

    def test_template_used(self):
        response = Client().get(reverse("learningplace:index"))
        self.assertTemplateUsed(response, 'learningplaceApp/index.html')

    def test_learningplace_save_a_POST_request(self):
        obj = contactUs.objects.create(name='Daryl')
        Client().post('/' + str(obj.id), data={'name': 'Daryl'})
        jumlah = contactUs.objects.filter(name='Daryl').count()
        self.assertEqual(jumlah, 1)
    
    #test for forms
    def test_form_is_valid(self):
        form_contactUs = contactUsForm({
            "name": "Richard",
            "email": "richardvier@gmail.com",
            "subject": "rating",
            "message": "nice, you made an interesting website"})
        self.assertTrue(form_contactUs.is_valid())
       
    def test_form_invalid(self):
        form_contactUs = contactUsForm(data={})
        self.assertFalse(form_contactUs.is_valid())


    #test for models
    def test_setup(self):
        self.contact = contactUs.objects.create(
            name="Darrel", email="darrelalvin@gmail.com", subject="rating web", message="9/10")
        
    def test_instance_created(self):
        self.assertEqual(contactUs.objects.count(), 0)
    
    
    #test for views 
    def test_setup(self):
        self.client = Client()
        self.product_create_view = reverse("learningplace:index")

    
    def test_GET_index(self):
        self.index = reverse("learningplace:index")
        response = self.client.get(self.index)
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'learningplaceApp/index.html')
    
    



