from django import forms

class AddSubscriber(forms.Form):
    attr_nama = {
        'type': 'text',
        'class': 'form-control',
        'placeholder': 'Nama...'
    }

    attr_email = {
        'type': 'text',
        'class': 'form-control',
        'placeholder': 'Email...'
    }

    attr_newsletter = {
        'type': 'checkbox',
        'class': 'form-control',
        'style':'width:20px;height:20px;'
    }

    nama_subscriber = forms.CharField(label='Masukkan nama:', max_length=50, required=True, widget=forms.TextInput(attrs=attr_nama))
    email_subscriber = forms.CharField(label='Masukkan email:', max_length=50, required=True, widget=forms.TextInput(attrs=attr_email))
    newsletter = forms.BooleanField(label='', required=False, widget=forms.CheckboxInput(attrs=attr_newsletter))

