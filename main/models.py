from django.db import models

class Subscriber(models.Model):
    nama_subscriber = models.CharField(max_length=50)
    email_subscriber = models.CharField(max_length=50)
    newsletter = models.BooleanField(default=False)
