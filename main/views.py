from django.shortcuts import render
from .forms import AddSubscriber
from .models import Subscriber

from learningplaceApp.forms import contactUsForm
from learningplaceApp.models import contactUs

def developer(request):
    return render(request, "main/developer-page.html")


def index(request):
    form = contactUsForm(request.POST or None)
 
    if form.is_valid():
        form.save()
        form = contactUsForm()

    context = {
        'form': form
    }
    return render(request, "main/index.html", context)

def Subscribe(request):
    subscriber_count = Subscriber.objects.count()
    response = {'add_subscriber' : AddSubscriber, 'subscriber_count' : subscriber_count}
    return render (request, 'main/subscribe.html', response)

def SaveSubscription(request):
    response = {'save_status':''}
    if (request.method == 'POST'):
        form = AddSubscriber(request.POST)
        if (form.is_valid()):
            nama_subscriber = form.cleaned_data['nama_subscriber'] 
            email_subscriber = form.cleaned_data['email_subscriber'] 
            newsletter = form.cleaned_data['newsletter']
            
            new_subscriber = Subscriber(
                nama_subscriber = nama_subscriber,
                email_subscriber = email_subscriber,
                newsletter = newsletter
            )

            new_subscriber.save()

            response['save_status'] = 'Subscribe berhasil!'
        else:
            response['save_status'] = 'Subscribe gagal'
    else:
        response['save_status'] = 'Something went wrong'

    return render (request, 'main/save.html', response)